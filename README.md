Practical Elevator
===================
A fork of [Elevator Saga](http://play.elevatorsaga.com/) adapted for educational events targeted at middle school pupils, organised by the Promoteam of Computing Science at the University of Groningen.

[Play it now!](https://cs.svcover.nl/elevator)
